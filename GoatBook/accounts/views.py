from django.core.mail import send_mail
from django.shortcuts import redirect
from django.contrib import auth, messages
from accounts.models import Token
from django.core.urlresolvers import reverse

def send_login_email(request):
    email = request.POST['email']
    token = Token.objects.create(email=email)
    url = request.build_absolute_uri(
        reverse('login') + '?token={uid}'.format(uid=str(token.uid))
    )
    message_body = 'login link: {url}'.format(url=url)
    send_mail(
        'login link',
        message_body,
        'noreply@superlists',
        [email],
    )
    
    messages.success(request,"sent")
    return redirect('/')

def login(request):
    user = auth.authenticate(uid=request.GET.get('token'))
    if user:
        auth.login(request, user)
    return redirect('/')