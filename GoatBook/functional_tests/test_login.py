import re
from django.core import mail

from .base import FunctionalTest

TEST_EMAIL = 'wasd@example.com'
SUBJECT = 'login link'


class LoginTest(FunctionalTest):

    def test_can_get_email_link_to_log_in(self):
        self.browser.get(self.server_url)
        self.browser.find_element_by_name('email').send_keys(TEST_EMAIL + '\n')

        body = self.browser.find_element_by_tag_name('body')
        self.assertIn('sent', body.text)

        email = mail.outbox[0]
        self.assertIn(TEST_EMAIL, email.to)
        self.assertEqual(email.subject, SUBJECT)

        self.assertIn(SUBJECT, email.body)
        url_search = re.search(r'http://.+/.+$', email.body)
        if not url_search:
            self.fail('Could not find url in email body:\n{}'.format(email.body))
        url = url_search.group(0)
        self.assertIn(self.server_url, url)

        self.browser.get(url)

        self.assert_logged_in(email=TEST_EMAIL)

        self.browser.find_element_by_link_text('logout').click()

        self.assert_logged_out(email=TEST_EMAIL)
