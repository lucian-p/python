'''
Created on Sep 29, 2016

@author: lpa
'''
from functional_tests.base import FunctionalTest
from lists.forms import DUPLICATE_ITEM_ERROR

class ItemValidationTest(FunctionalTest):
    
    def get_error_element(self):
        return self.browser.find_element_by_css_selector('.has-error')
    
    def test_cannot_add_blank_entry(self):
        # user story:
        # ionel intra pe site si icearca sa adauge din greseala un item blank
        # ionel apasa enter cand text-box-ul e gol
        self.browser.get(self.live_server_url)
        self.get_item_input_box().send_keys('\n')
        
        # pagina se refresh si ii arata un mesaj de eroare informand-ul de greseala pe care a facut-o
        error = self.get_error_element()
        self.assertEqual(error.text, 'no empty items')
                
        # ionel se conformeaza si introduce un item cu continut
        self.get_item_input_box().send_keys('okok\n')
        self.chech_for_row_in_list_table('1: okok')
        
        # ionel, incercand sa bata sistemul, incearca sa introduca un item fara continut din nou
        self.get_item_input_box().send_keys('\n')
        
        # ionel primeste un mesaj de eroare similar cu cel anterior
        self.chech_for_row_in_list_table('1: okok')
        error = self.get_error_element()
        self.assertEqual(error.text, 'no empty items')
        
        # ionel nu e atat de destept pe cat se crede...
        self.get_item_input_box().send_keys('am inteles\n')
        self.chech_for_row_in_list_table('1: okok')
        self.chech_for_row_in_list_table('2: am inteles')
        
    def test_cannot_add_duplicate_items(self):
        # ionel intra pe site si face o lista noua
        self.browser.get(self.server_url)
        self.get_item_input_box().send_keys('chestii\n')
        self.chech_for_row_in_list_table('1: chestii')
    
        # ionel incearca sa adauge acelasi item din greseaa
        self.get_item_input_box().send_keys('chestii\n')
    
        # ionel primeste un mesaj de eroare
        self.chech_for_row_in_list_table('1: chestii')
        error = self.get_error_element()
        self.assertEqual(error.text, DUPLICATE_ITEM_ERROR)
        
        
    def test_error_messages_are_cleared_on_input(self):
        # ionel face o lista noua care crapa
        self.browser.get(self.server_url)
        self.get_item_input_box().send_keys('\n')
        error = self.get_error_element()
        self.assertTrue(error.is_displayed())  
    
        # ionel scrie ceva si se asteapta ca eroarea sa dispara
        self.get_item_input_box().send_keys('w')
    
        # ionel vede ca eroarea a disparut
        error = self.get_error_element()
        self.assertFalse(error.is_displayed())