'''
Created on Sep 29, 2016

@author: lpa
'''
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from functional_tests.base import FunctionalTest

class NewVistorTests(FunctionalTest):        
    
    def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get(self.server_url)
        self.browser.set_window_size(1024, 768)
        self.assertIn('todo', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('todo',header_text)
        
        inputbox = self.get_item_input_box()
        self.assertEqual(inputbox.get_attribute('placeholder'), 'enter item')
        
        inputbox.send_keys('buy stuff')
        inputbox.send_keys(Keys.ENTER)
        
        # input box is nicely centered
        inputbox = self.get_item_input_box()
        self.assertAlmostEqual(inputbox.location['x'] + inputbox.size['width'] / 2, 512, delta = 10)
        
        list_url = self.browser.current_url
        self.assertRegex(list_url, '/lists/.+')
        self.chech_for_row_in_list_table('1: buy stuff')

        inputbox = self.get_item_input_box()
        inputbox.send_keys('use stuff')
        inputbox.send_keys(Keys.ENTER)
        self.chech_for_row_in_list_table('1: buy stuff')
        self.chech_for_row_in_list_table('2: use stuff')
        
        self.browser.refresh()
        self.browser.quit()
        self.browser = webdriver.Chrome()
        self.browser.get(self.server_url)
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('buy stuff', page_text)
        self.assertNotIn('use stuff', page_text)

        inputbox = self.get_item_input_box()
        inputbox.send_keys('buy other things')
        inputbox.send_keys(Keys.ENTER)
        new_list_url =self.browser.current_url 
        self.assertRegex(list_url, '/lists/.+')
        self.assertNotEqual(new_list_url, list_url)
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('buy stuff', page_text)
        self.assertIn('buy other things', page_text)